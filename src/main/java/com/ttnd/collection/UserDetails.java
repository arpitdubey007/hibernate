package com.ttnd.collection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity(name="collection_user_detail")
public class UserDetails {
	@Id @GeneratedValue
	private int id;
	
	private String name;
	private String motherToung;
	private String mobileNo;
	
	@ElementCollection
	List<Vehicle> vehicleList=new ArrayList<Vehicle>();

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMotherToung() {
		return motherToung;
	}
	public void setMotherToung(String motherToung) {
		this.motherToung = motherToung;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public List<Vehicle> getVehicleList() {
		return vehicleList;
	}
	public void setVehicleList(List<Vehicle> vehicleList) {
		this.vehicleList = vehicleList;
	}
	
	
}
