package com.ttnd.collection;

import javax.persistence.Embeddable;

@Embeddable
public class Vehicle {
	
	private int vehicle_id;
	private String vehicle_name;
	
	public int getVehicle_id() {
		return vehicle_id;
	}
	public void setVehicle_id(int vehicle_id) {
		this.vehicle_id = vehicle_id;
	}
	public String getVehicle_name() {
		return vehicle_name;
	}
	public void setVehicle_name(String vehicle_name) {
		this.vehicle_name = vehicle_name;
	}
	
}
