package com.ttnd.collection;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class Collection_Main {
	
public static void main(String args[]){
	
	
	Vehicle vehicle1=new Vehicle();
	Vehicle vehicle2=new Vehicle();
	Vehicle vehicle3=new Vehicle();
	
	vehicle1.setVehicle_id(1);
	vehicle1.setVehicle_name("Bus");
	vehicle2.setVehicle_id(2);
	vehicle2.setVehicle_name("Car");
	vehicle3.setVehicle_id(3);
	vehicle3.setVehicle_name("Jeep");
	
	
	/*List<Vehicle> vehicleList=new ArrayList<Vehicle>();
	vehicleList.add(vehicle1);
	vehicleList.add(vehicle2);
	vehicleList.add(vehicle3);*/
	
	UserDetails userDetails=new UserDetails();
	userDetails.setName("Arpit Dubey");
	userDetails.setMotherToung("Hindi");
	userDetails.setMobileNo("7042752112");
	userDetails.getVehicleList().add(vehicle1);
	userDetails.getVehicleList().add(vehicle2);
	userDetails.getVehicleList().add(vehicle3);
	
	//SessionFactory sessionFactory =	GivesFactory.getSessionFactoryInstance();
	SessionFactory sessionFactory =new AnnotationConfiguration().configure().buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	session.save(userDetails);
	session.getTransaction().commit();	
	session.close();
		
	
}

}
