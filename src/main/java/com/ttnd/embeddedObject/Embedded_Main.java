package com.ttnd.embeddedObject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class Embedded_Main {
	
public static void main(String args[]){
	
	Address address=new Address();
	address.setCity("Kannauj");
	address.setPincode("209725");
	address.setState("UttarPradesh");
	address.setStreet("MithaiWaliGali");
	
	UserDetails userDetails=new UserDetails();
	userDetails.setName("Arpit Dubey");
	userDetails.setMotherToung("Hindi");
	userDetails.setMobileNo("7042752112");
	userDetails.setAddresss(address);
	
	
	//SessionFactory sessionFactory =	GivesFactory.getSessionFactoryInstance();
	SessionFactory sessionFactory =new AnnotationConfiguration().configure().buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	session.save(userDetails);	
	session.getTransaction().commit();	
	session.close();
		
	
}

}
