package com.ttnd.embeddedObject;
import javax.persistence.Embeddable;

//value-object----no meaning its own
//to tell hibernate no need to create seperate table for this value object
@Embeddable
public class Address {
	private String street;
	private String city;
	private String state;
	private String pincode;

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	
}
