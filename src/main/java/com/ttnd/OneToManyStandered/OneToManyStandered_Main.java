package com.ttnd.OneToManyStandered;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class OneToManyStandered_Main {
	
public static void main(String args[]){
	
	Vehicle vehicle1=new Vehicle();
	vehicle1.setVehicle_name("HondaCity");
	Vehicle vehicle2=new Vehicle();
	vehicle2.setVehicle_name("jaguar");
	
	UserDetails userDetails=new UserDetails();
	userDetails.setName("Arpit Dubey");
	userDetails.setEmployeer("Intelegencia");
	userDetails.setMobileNo("7042752112");
	
	userDetails.getVehicleList().add(vehicle1);
	userDetails.getVehicleList().add(vehicle2);
	vehicle1.setUser(userDetails);
	vehicle2.setUser(userDetails);
	
	
	
	
	
	
	SessionFactory sessionFactory =new Configuration().configure().buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	
	session.save(userDetails);	
	session.save(vehicle1);
	session.save(vehicle2);
	
	session.getTransaction().commit();	
	session.close();
		
	
}

}
