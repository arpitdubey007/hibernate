package com.ttnd.OneToManyStdAvoidMapping;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class OneToManyOneToManyStdAvoidMapping_Main {
	
public static void main(String args[]){
	
	Vehicle vehicle1=new Vehicle();
	vehicle1.setVehicle_name("Bike");
	Vehicle vehicle2=new Vehicle();
	vehicle2.setVehicle_name("Car");
	
	UserDetails userDetails=new UserDetails();
	userDetails.setName("Sankalp Shukla");
	userDetails.setEmployeer("Oracle");
	userDetails.setMobileNo("9369051648");
	
	
	
	userDetails.getVehicleList().add(vehicle1);
	userDetails.getVehicleList().add(vehicle2);
	vehicle1.setUser(userDetails);
	vehicle2.setUser(userDetails);
	
	
	
	
	
	
	SessionFactory sessionFactory =new Configuration().configure().buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	
	session.save(userDetails);	
	session.save(vehicle1);
	session.save(vehicle2);
	
	session.getTransaction().commit();	
	session.close();
		
	
}

}
