package com.ttnd.oneToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class OneToOne_Main {
	
public static void main(String args[]){
	
	Vehicle vehicle=new Vehicle();
	vehicle.setVehicle_name("HondaCity");
	
	
	
	UserDetails userDetails=new UserDetails();
	userDetails.setName("Arpit Dubey");
	userDetails.setEmployeer("Intelegencia");
	userDetails.setMobileNo("7042752112");
	userDetails.setVehicle(vehicle);
	
	
	
	SessionFactory sessionFactory =new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	session.save(userDetails);	
	session.save(vehicle);
	session.getTransaction().commit();	
	session.close();
		
	
}

}
