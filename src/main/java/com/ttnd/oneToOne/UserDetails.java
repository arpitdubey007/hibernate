package com.ttnd.oneToOne;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity(name="onetoone_user_detail")
public class UserDetails {
	
	@Id @GeneratedValue
	private int id;
	private String name;
	private String employeer;
	private String mobileNo;

	
	// no mapping table will be created in one to one case..
	//& have to save in session before committing session...
	//new jointcolumn Column will be add in database onetoone_user_detail table 
	
	
	@OneToOne
	@JoinColumn(name="vehile_id")
	private Vehicle vehicle;
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmployeer() {
		return employeer;
	}


	public void setEmployeer(String employeer) {
		this.employeer = employeer;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public Vehicle getVehicle() {
		return vehicle;
	}


	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}




	
}
