package com.ttnd.ManyToMany;

import java.util.ArrayList;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;



@Entity(name="manytomany_user_detail")
public class UserDetails {
	
	@Id @GeneratedValue
	private int id;
	private String name;
	private String employeer;
	private String mobileNo;

	@ManyToMany(mappedBy="userList")
	List<Vehicle> VehicleList=new ArrayList<Vehicle>();
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmployeer() {
		return employeer;
	}


	public void setEmployeer(String employeer) {
		this.employeer = employeer;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}


	public List<Vehicle> getVehicleList() {
		return VehicleList;
	}


	public void setVehicleList(List<Vehicle> vehicleList) {
		VehicleList = vehicleList;
	}


	
}
