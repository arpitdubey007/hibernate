package com.ttnd.ManyToMany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class ManyToMany_Main {
	
public static void main(String args[]){
	
	Vehicle vehicle1=new Vehicle();
	vehicle1.setVehicle_name("HondaCity");
	Vehicle vehicle2=new Vehicle();
	vehicle2.setVehicle_name("jaguar");
	
	UserDetails user1=new UserDetails();
	user1.setName("Arpit Dubey");
	user1.setEmployeer("Intelegencia");
	user1.setMobileNo("7042752112");
	
	UserDetails user2=new UserDetails();
	user2.setName("Rohit Sharma");
	user2.setEmployeer("Cisco");
	user2.setMobileNo("9838624181");
	
	
	/*user1.getVehicleList().add(vehicle1);
	user1.getVehicleList().add(vehicle2);
	user2.getVehicleList().add(vehicle1);
	user2.getVehicleList().add(vehicle2);*/
	
	vehicle1.getUserList().add(user1);
	vehicle1.getUserList().add(user2);
	vehicle2.getUserList().add(user1);
	vehicle2.getUserList().add(user2);
	
	SessionFactory sessionFactory =new Configuration().configure().buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	
	session.save(user1);	
	session.save(user2);	
	session.save(vehicle1);
	session.save(vehicle2);
	
	session.getTransaction().commit();	
	session.close();
		
	
}

}
