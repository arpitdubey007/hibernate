package com.ttnd.ManyToMany;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;


@Entity(name="manytomany_vehicle")
public class Vehicle {
	@Id @GeneratedValue
	private int vehicle_id;
	public List<UserDetails> getUserList() {
		return userList;
	}
	public void setUserList(List<UserDetails> userList) {
		this.userList = userList;
	}
	private String vehicle_name;
	
	
	
	
	@ManyToMany
	List<UserDetails> userList=new ArrayList<UserDetails>();
	

	public int getVehicle_id() {
		return vehicle_id;
	}
	public void setVehicle_id(int vehicle_id) {
		this.vehicle_id = vehicle_id;
	}
	public String getVehicle_name() {
		return vehicle_name;
	}
	public void setVehicle_name(String vehicle_name) {
		this.vehicle_name = vehicle_name;
	}
	
}
