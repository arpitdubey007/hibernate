package com.ttnd.manyToOne;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity(name="manytoone_user_detail")
public class UserDetails {
	
	@Id @GeneratedValue
	private int id;
	private String name;
	private String employeer;
	private String mobileNo;

	
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmployeer() {
		return employeer;
	}


	public void setEmployeer(String employeer) {
		this.employeer = employeer;
	}


	public String getMobileNo() {
		return mobileNo;
	}


	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}




	
}
