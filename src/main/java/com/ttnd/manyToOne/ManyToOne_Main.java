package com.ttnd.manyToOne;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import common.utility.GivesFactory;

public class ManyToOne_Main {
	
public static void main(String args[]){
	
	Vehicle vehicle1=new Vehicle();
	vehicle1.setVehicle_name("HondaCity");
	
	Vehicle vehicle2=new Vehicle();
	vehicle2.setVehicle_name("jaguar");
	
	UserDetails user1=new UserDetails();
	user1.setName("Arpit Dubey");
	user1.setEmployeer("Intelegencia");
	user1.setMobileNo("7042752112");
	
	vehicle1.setUser(user1);
	vehicle2.setUser(user1);
	
	SessionFactory sessionFactory =new Configuration().configure().buildSessionFactory();
 	Session session =sessionFactory.openSession();
	session.beginTransaction();
	
	session.save(user1);	
	session.save(vehicle1);
	session.save(vehicle2);
	
	session.getTransaction().commit();	
	session.close();
		
	
}

}
