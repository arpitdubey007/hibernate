package common.utility;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class GivesFactory {
	
	private static SessionFactory sessionFactory = null;
	
	private GivesFactory()
	{
		sessionFactory = new Configuration().configure().buildSessionFactory();
	}
	
	   public static SessionFactory getSessionFactoryInstance() {
		      if(sessionFactory == null) {
		    	  sessionFactory = new Configuration().configure().buildSessionFactory();
		      }
		      
		      
		      return sessionFactory;
		   }
}
